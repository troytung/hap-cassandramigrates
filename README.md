hap-cassandramigrates
==============

Cassandra database migration scripts
數據庫轉移腳本，所有 Cassandra 數據庫 schema 跟參考資料的修改都必須被記錄在此。
腳本放置在 src/main/resources/migration 這個目錄，並以下列命名方式

yyyymmddss__title.cql
- yyyymmddss 是版號，其中 yyyymmddss 是日期加上兩位數序號，例如 2017090401
- 版號之後是兩個底線
- title 是此腳本的簡短描述

參考資料
==============
https://github.com/patka/cassandra-migration

本地測試
==============

```
./gradlew bootRun
```
