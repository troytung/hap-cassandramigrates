//package com.happypay.migration;
//
//import com.datastax.driver.core.Cluster;
//import org.cognitor.cassandra.migration.Database;
//import org.cognitor.cassandra.migration.MigrationRepository;
//import org.cognitor.cassandra.migration.MigrationTask;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.boot.builder.SpringApplicationBuilder;
//
//@SpringBootApplication(scanBasePackages = "com.happypay.migration")
//public class App {
//    public static void main(String[] args) {
////        SpringApplication.run(Application.class);
////        new SpringApplicationBuilder(App.class).web(false).run(args);
//        String serverIP = "127.0.0.1";
//        String keyspace = "happypay";
//
//        Cluster cluster = Cluster.builder()
//                .addContactPoints(serverIP)
//                .build();
//
////        Session session = cluster.connect(keyspace);
//        // you are now connected to the cluster, congrats!
//
//        Database database = new Database(cluster, keyspace);
//        MigrationTask migration = new MigrationTask(database, new MigrationRepository("migration"));
//        migration.migrate();
//    }
//}
