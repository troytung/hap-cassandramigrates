package com.happypay.migration;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
//        SpringApplication.run(Application.class);
        new SpringApplicationBuilder(Application.class).web(false).run(args).close();
    }
}
